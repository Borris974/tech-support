FROM node:16.3.0-alpine3.11 AS node
ARG PROXYPARAMS
# RUN echo ${PROXYPARAMS}

RUN mkdir /app
WORKDIR /app

COPY . .
#COPY --from=get-source-step /home/src/tessi-food-back .

RUN npm install --loglevel verbose

RUN npm run build --prod
# WORKDIR /app/dist
# RUN node main.js
CMD ["node", "dist/main"]
