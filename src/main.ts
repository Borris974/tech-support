import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerCustomOptions,
} from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import { Logger } from '@nestjs/common'; '@nestjs/common';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  const options = new DocumentBuilder()
    .setTitle('Tech support API')
    .setDescription('Swagger documentation for Tech support API')
    .setVersion('1.0')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'JWT',
    )
    .build();

  const customOptions = {
    customSiteTitle: 'Tech support API Swagger UI',
  } as SwaggerCustomOptions;
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document, customOptions);

  app.use(helmet()); 

  await app.listen(3000);
  logger.log('Application démarrée sur le port 3000')
}
bootstrap();
