import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  OnModuleInit,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user-dto';
import { toDetailedUserDto, toSimpleUserDto, UserDto } from './dto/user-dto';
import { User } from './user.model';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './dto/update-user-dto';
import { IssueService } from 'src/issue/issue.service';
import { UserRole } from './enums/user-role.enum';
import { Logger } from '@nestjs/common';

@Injectable()
export class UserService implements OnModuleInit {
  private logger = new Logger('UserService');
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    @Inject(forwardRef(() => IssueService))
    private readonly issueService: IssueService,
  ) {}

  async onModuleInit() {  
    let defaultAdminUser;
    try {
      defaultAdminUser = await this.findUser({
        email: process.env.DEFAULT_ADMIN_EMAIL,
      });
    } catch (error) {
      console.info(error);
    }
    if (!defaultAdminUser) {
      defaultAdminUser = new this.userModel({
        firstname: 'Admin',
        lastname: 'ADMIN',
        email:process.env.DEFAULT_ADMIN_EMAIL,
        password: await bcrypt.hash(process.env.DEFAULT_ADMIN_PASSWORD, 10),
        role: UserRole.ADMIN,
      });
      defaultAdminUser.save();
    }
  }

  async getUsers(): Promise<Array<UserDto>> {
    const result = await this.findUsers();
    return result.map((user) => {
      return toSimpleUserDto(user);
    }) as Array<UserDto>;
  }

  async findUsers(filterQuery = {}, ...selectFields): Promise<any> {
    const query = this.userModel.find(filterQuery);
    if (selectFields.length > 0) {
      query.select(selectFields);
    }

    return await query.exec();
  }

  async findUser(
    userParam: { _id: string } | { email: string },
  ): Promise<User> {
    let result: User;
    try {
      result = await this.userModel
        .findOne(userParam)
        .populate('assignedIssues');
    } catch (error) {
      throw new NotFoundException(
        `L'utilisateur ${Object.keys(userParam)[0]} : ${
          Object.values(userParam)[0]
        }"  n'a pas été trouvé`,
      );
    }
    if (!result) {
      throw new NotFoundException(
        `L'utilisateur' "${Object.keys(userParam)[0]} : ${
          Object.values(userParam)[0]
        }" n'a pas été trouvé`,
      );
    }
    return result;
  }

  async getUser(idUser: string): Promise<UserDto> {
    const user: User = await this.findUser({ _id: idUser });
    return toDetailedUserDto(user);
  }

  async createUser(createUserDto: CreateUserDto): Promise<UserDto> {
    const { firstname, lastname, email, password, role } = createUserDto;
    const newUser = new this.userModel({
      firstname,
      lastname,
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });
    return toDetailedUserDto(await newUser.save());
  }

  async createIssue(userId, issueDto, filesPaths) {
    return this.issueService.createIssue(userId, issueDto, filesPaths);
  }

  async updateUser(
    idUser: string,
    updateUser: UpdateUserDto,
  ): Promise<UserDto> {
    const { firstname, lastname } = updateUser;
    const user = await this.findUser({ _id: idUser });

    if (firstname) {
      user.firstname = firstname;
    }
    if (lastname) {
      user.lastname = lastname;
    }

    await user.save();
    return this.getUser(idUser);
  }

  async assignUserToIssue(userId: string, issueId: string) {
    return this.userModel.findByIdAndUpdate(userId, {
      $addToSet: { assignedIssues: issueId },
    });
  }

  async removeUserFromIssue(userId: string, issueId: string) {
    return this.userModel.findByIdAndUpdate(userId, {
      $pull: { assignedIssues: issueId },
    });
  }

  async deleteUser(userId: string): Promise<any> {
    const result = await this.userModel.deleteOne({ _id: userId });
    if (result.deletedCount === 0) {
      throw new NotFoundException(
        `Impossible de trouver l'utilisateur ${userId}`,
      );
    }
    return result;
  }
}
