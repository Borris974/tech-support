import { forwardRef, Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.model';
import { IssueService } from 'src/issue/issue.service';
import { IssueModule } from 'src/issue/issue.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    forwardRef(() => IssueModule),
    forwardRef(() => AuthModule),
  ],
  controllers: [UserController],
  providers: [UserService, IssueService],
  exports: [MongooseModule, UserService],
})
export class UserModule {}
