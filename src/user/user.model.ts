import * as mongoose from 'mongoose';
import { Issue } from 'src/issue/issue.model';
import { UserRole } from './enums/user-role.enum';

export const UserSchema = new mongoose.Schema({
  firstname: { type: String, required: true, trim: true },
  lastname: { type: String, required: true, trim: true, uppercase: true },
  email: {
    type: String,
    required: true,
    unique: true,
    dropDups: true,
    immutable: true,
    trim: true,
  },
  password: { type: String, required: true, trim: true },
  role: { type: String, required: true, trim: true },
  assignedIssues: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Issue',
        
      },
    ],
  },
  createdIssues: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Issue',
      },
    ],
  },
});

export interface User extends mongoose.Document {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  role: UserRole;
  assignedIssues: Array<Issue>;
  createdIssues: Array<Issue>;
}
