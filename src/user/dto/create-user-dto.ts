import { ApiProperty } from '@nestjs/swagger';
import {
  NotEquals,
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  Matches,
  IsIn,
} from 'class-validator';
import { UserRole } from '../enums/user-role.enum';

export class CreateUserDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  firstname: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  lastname: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @MinLength(8, {
    message: `Le mot de passe doit comporter au moins 8 caractères`,
  })
  @Matches(/(?=.*[A-Z].*[A-Z]).*$/, {
    message: `Le mot de passe doit avoir au minimum 2 majuscules`,
  })
  @Matches(/(?=.*[!@#$&*]).*$/, {
    message: `Le mot de passe doit avoir au minimum 1 caractère spécial`,
  })
  @Matches(/(?=.*[0-9].*[0-9]).*$/, {
    message: `Le mot de passe doit avoir au minimum 2 chiffres`,
  })
  @Matches(/(?=.*[a-z].*[a-z].*[a-z]).*$/, {
    message: `Le mot de passe doit avoir au minimum 3 minuscules`,
  })
  password: string;

  @ApiProperty({ required: true, enum: UserRole  })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsIn([UserRole.DEVELOPER, UserRole.MANAGER, UserRole.ADMIN])
  role: UserRole;
}
