import {
  IsEmail,
  IsString,
  IsMongoId,
  IsArray,
  IsOptional,
  NotEquals,
  IsIn,
  IsNotEmpty,
} from 'class-validator';
import {
  IssueDto,  
  toSimpleIssueDto,
} from 'src/issue/dto/issue-dto';
import { Issue } from 'src/issue/issue.model';
import { UserRole } from '../enums/user-role.enum';
import { User } from '../user.model';

export class UserDto {
  @IsMongoId()
  id: any;

  @IsString()
  firstname: string;

  @IsString()
  lastname: string;

  @IsString()
  @IsEmail()
  email: string;

  @IsArray()
  @IsOptional()
  assignedIssues: Array<IssueDto>;

  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsIn([UserRole.DEVELOPER, UserRole.MANAGER, UserRole.ADMIN])
  role: UserRole;
}

export const toSimpleUserDto = (user: User): UserDto => {
  const { _id, firstname, lastname, email, role } = user;
  return {
    id: _id,
    firstname,
    lastname,
    email,
    role,
  } as UserDto;
};

export const toDetailedUserDto = (user: User): UserDto => {
  const { _id, firstname, lastname, email, assignedIssues, role } = user;
  return {
    id: _id,
    firstname,
    lastname,
    email,
    assignedIssues: assignedIssues.map((issue) => {
      return toSimpleIssueDto(issue);
    }) as Array<IssueDto>,
    role,
  };
};
