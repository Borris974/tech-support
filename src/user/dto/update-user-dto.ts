import { ApiProperty } from '@nestjs/swagger';
import { NotEquals, IsNotEmpty, IsString, IsIn, IsOptional } from 'class-validator';


export class UpdateUserDto {

  @ApiProperty({ required: false})
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsOptional()
  firstname: string;

  @ApiProperty({ required: false})
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsOptional()
  lastname: string;


}