import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user-dto';
import { UserDto } from './dto/user-dto';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiParam, ApiProperty, ApiTags } from '@nestjs/swagger';
import { CreateIssueDto } from 'src/issue/dto/create-issue-dto';
import { IssueService } from 'src/issue/issue.service';
import { diskStorage } from 'multer';
import { FilesInterceptor } from '@nestjs/platform-express';
import { UpdateUserDto } from './dto/update-user-dto';
import { extname } from 'path';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Roles } from 'src/auth/decorators/role.decorator';
import { UserRole } from './enums/user-role.enum';

@ApiTags('Users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles(UserRole.ADMIN, UserRole.MANAGER)
  @ApiBearerAuth('JWT')
  async getUsers(): Promise<Array<UserDto>> {
    return this.userService.getUsers();
  }

  @Get(':id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async getIssue(@Param('id') id: string): Promise<UserDto> {
    return this.userService.getUser(id);
  }

  @Post()
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles(UserRole.ADMIN, UserRole.MANAGER)
  @ApiBearerAuth('JWT')
  async createUser(@Body() userDto: CreateUserDto): Promise<UserDto> {
    return this.userService.createUser(userDto);
  }

  @Patch(':id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async updateUser(
    @Param('id') id: string,
    @Body() updateUser: UpdateUserDto,
  ): Promise<UserDto> {
    return this.userService.updateUser(id, updateUser);
  }

  @Delete(':id')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles(UserRole.ADMIN)
  @ApiBearerAuth('JWT')
  async deleteUser(@Param('id') id: string): Promise<any> {
    return this.userService.deleteUser(id);
  }

  @Post(':id/issues')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles(UserRole.ADMIN, UserRole.MANAGER)
  @ApiBearerAuth('JWT')
  @ApiParam({ name: 'id', description: `user's Mongo ID`, required: true })
  @UseInterceptors(
    FilesInterceptor('files', 3, {
      storage: diskStorage({
        destination: `./public/issues-attachments`,
        filename: (req, file, cb) => {
          if (file.mimetype.match(/(application\/pdf|image\/jpg|image\/jpeg|image\/png|image\/gif|text\/plain)$/)) {
          cb(null, `att_${Date.now()}${extname(file.originalname)}`);
        } else {
          cb(
            new HttpException(
              `Type de fichier non autorisé : ${extname(file.originalname)}`,
              HttpStatus.BAD_REQUEST,
            ),
            file.originalname,
          );
        }
        },
      }),
    }),
  )
  async createIssue(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Param('id') userId: string,
    @Body() issueDto: CreateIssueDto,
  ) {
    const filesPaths = [];
    if (files) {
      files.forEach((file) => {
        filesPaths.push(`issues-attachments/${file.filename}`);
      });
    }
    return this.userService.createIssue(userId, issueDto, filesPaths);
  }
}
