import {
  forwardRef,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { AuthCredentialsDto } from './dto/auth-credentials-dto';
import * as bcrypt from 'bcrypt';
import { toSimpleUserDto } from 'src/user/dto/user-dto';
import { User } from 'src/user/user.model';
import { JwtPayload } from './interfaces/jwt.payload.interface';
import { JwtService } from '@nestjs/jwt';
import { Logger } from '@nestjs/common';

@Injectable()
export class AuthService {
  private logger = new Logger('UserService');
  constructor(
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  async signIn(authCredentialsDto: AuthCredentialsDto): Promise<any> {
    const { email, password } = authCredentialsDto;
    let user: User;
    try {
      user = await this.userService.findUser({ email: email });
    } catch (error) {
      this.logger.error(
        `Erreur lors de l'authentification de ${email}'`,
        error.stack,
      );
      throw new UnauthorizedException('Identifiants incorrects');
    }

    if (user && (await bcrypt.compare(password, user.password))) {
      return {
        user: toSimpleUserDto(user),
        accessToken: this.createToken(user),
      };
    } else {
      throw new UnauthorizedException('Identifiants incorrects');
    }
  }

  private createToken(user: User): string {
    const { id, firstname, lastname, email } = user;
    const payload: JwtPayload = { id, firstname, lastname, email };

    return this.jwtService.sign(payload, {
      expiresIn: process.env.EXPIRES_IN,
      secret: process.env.SECRET,
    });
  }
}
