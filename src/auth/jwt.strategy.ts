import { forwardRef, Inject, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { User } from 'src/user/user.model';
import { UserService } from 'src/user/user.service';
import { JwtPayload } from './interfaces/jwt.payload.interface';

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(forwardRef(() => UserService)) readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.SECRET,
    });
  }

  async validate(payload: JwtPayload): Promise<User> { 
    const { id } = payload;
    const user: User = await this.userService.findUser({ _id: id });
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
