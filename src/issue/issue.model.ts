import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { User } from 'src/user/user.model';
import { IssueStatus } from './enums/status.enum';

export const IssueSchema = new mongoose.Schema({
  createdAt: { type: Date, required: true },
  updatedAt: { type: Date, required: true },
  ref: { type: String, required: true, trim: true },
  title: { type: String, required: true, trim: true },
  content: { type: String, required: true, trim: true },
  status: { type: String, required: true, trim: true },
  attachments: { type: Array },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  assignees: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
});

export interface Issue extends mongoose.Document {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  ref: string;
  title: string;
  content: string;
  status: IssueStatus;
  attachments: Array<string>;
  creator: User;
  assignees: Array<User>;
}
