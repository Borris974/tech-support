import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from 'src/auth/auth.module';
import { UserModule } from 'src/user/user.module';
import { UserService } from 'src/user/user.service';
import { IssueController } from './issue.controller';
import { IssueSchema } from './issue.model';
import { IssueService } from './issue.service';

@Module({
  imports: [    
    MongooseModule.forFeature([{ name: 'Issue', schema: IssueSchema }]),    
    forwardRef(() => UserModule),
    forwardRef(() => AuthModule),
  ],
  controllers: [IssueController],
  providers: [IssueService, UserService],
  exports: [MongooseModule, IssueService],
})
export class IssueModule {}
