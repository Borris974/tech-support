import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Issue } from './issue.model';
import { UserService } from 'src/user/user.service';
import { AssignUserDto } from './dto/assign-user-dto';
import { CreateIssueDto } from './dto/create-issue-dto';
import {
  IssueDto,
  toSimpleIssueDto,
  toDetailedIssueDto,
} from './dto/issue-dto';
import { IssueFiltersDto } from './dto/issue-filters-dto';
import { UpdateIssueDto } from './dto/update_issue-dto';
import { UpdateIssueStatusDto } from './dto/update_issue-status-dto';
import { IssueStatus } from './enums/status.enum';
import { DeleteIssueAttachmentDto } from './dto/delete-issue-attachment-dto';
import * as fs from 'fs';
import { Logger } from '@nestjs/common';

@Injectable()
export class IssueService {
  private logger = new Logger('IssueService');
  constructor(
    @InjectModel('Issue') private readonly issueModel: Model<Issue>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  async getIssues(filters: IssueFiltersDto): Promise<Array<IssueDto>> {
    const { id, ref, title, content, creator } = filters;
    const query = this.issueModel.find();

    if (creator) {
      const usersIds = await this.userService.findUsers(
        {
          $or: [
            { firstname: { $regex: creator, $options: 'i' } },
            { lastname: { $regex: creator, $options: 'i' } },
            { email: { $regex: creator, $options: 'i' } },
          ],
        },
        'id',
      );
      query.where('creator').in(usersIds);
    }

    if (id) {
      query.where('_id').equals(id);
    }

    if (ref) {
      query.where('ref').equals({ $regex: ref, $options: 'i' });
    }

    if (title) {
      query.where('title').equals({ $regex: title, $options: 'i' });
    }

    if (content) {
      query.where('content').equals({ $regex: content, $options: 'i' });
    }

    try {
      const result = await query.populate(['creator']).exec();
      return result.map((issue) => {
        return toSimpleIssueDto(issue);
      }) as Array<IssueDto>;
    } catch (error) {
      this.logger.error(
        `Erreur lors de lé récupération des tickets`,
        error.stack,
      );
      throw error;
    }
  }

  async getAdminStats(): Promise<any> {
    try {
      const response = await this.issueModel.aggregate([
        {
          $facet: {
            total_issues: [{ $count: 'total_issues' }],
            opened_issues: [
              { $match: { status: IssueStatus.OPENED } },
              { $count: 'opened_issues' },
            ],
            in_progress_issues: [
              { $match: { status: IssueStatus.IN_PROGRESS } },
              { $count: 'in_progress_issues' },
            ],
            closed_issues: [
              { $match: { status: IssueStatus.CLOSED } },
              { $count: 'closed_issues' },
            ],
            unassigned_issues: [
              { $match: { assignees: [] } },
              { $count: 'unassigned_issues' },
            ],
            assigned_issues: [
              { $match: { assignees: { $exists: true, $not: { $size: 0 } } } },
              { $count: 'assigned_issues' },
            ],
          },
        },
        {
          $project: {
            total_issues: { $arrayElemAt: ['$total_issues.total_issues', 0] },
            opened_issues: {
              $arrayElemAt: ['$opened_issues.opened_issues', 0],
            },
            in_progress_issues: {
              $arrayElemAt: ['$in_progress_issues.in_progress_issues', 0],
            },
            closed_issues: {
              $arrayElemAt: ['$closed_issues.closed_issues', 0],
            },
            unassigned_issues: {
              $arrayElemAt: ['$unassigned_issues.unassigned_issues', 0],
            },
            assigned_issues: {
              $arrayElemAt: ['$assigned_issues.assigned_issues', 0],
            },
          },
        },
      ]);

      return response;
    } catch (error) {
      this.logger.error(
        `Erreur lors de lé récupération des stats`,
        error.stack,
      );
      throw error;
    }
  }

  async findIssue(idIssue: string): Promise<Issue> {
    let issue: Issue;
    try {
      const query = this.issueModel
        .findById(idIssue)
        .populate(['assignees', 'creator', 'attachments']);
      issue = await query.exec();
    } catch (error) {
      this.logger.error(
        `Erreur lors de la récupération du ticket`,
        error.stack,
      );
      throw new NotFoundException(
        `Le ticket avec l'id ${idIssue} n'existe pas`,
      );
    }
    if (!issue) {
      throw new NotFoundException(
        `Le ticket avec l'id ${idIssue} n'existe pas`,
      );
    }
    return issue;
  }

  async getIssue(idIssue: string): Promise<IssueDto> {
    const issue: Issue = await this.findIssue(idIssue);
    return toDetailedIssueDto(issue);
  }

  async createIssue(
    creatorId: string,
    createIssueDto: CreateIssueDto,
    filesPaths: Array<string>,
  ): Promise<IssueDto> {
    const creator = await this.userService.findUser({ _id: creatorId });
    try {
      const newIssue = new this.issueModel(createIssueDto);
      newIssue.creator = creator;
      newIssue.ref = `ISS_${Date.now()}`;
      newIssue.createdAt = new Date();
      newIssue.updatedAt = new Date();
      newIssue.status = IssueStatus.OPENED;
      newIssue.attachments = filesPaths;
      return toDetailedIssueDto(await newIssue.save());
    } catch (error) {
      this.logger.error(`Erreur lors de la création du ticket`, error.stack);
      throw error;
    }
  }
  async addIssueAttachments(
    issueId: string,
    filesPaths: Array<string>,
  ): Promise<IssueDto> {
    const issue = await this.findIssue(issueId);
    try {
      issue.attachments = [...issue.attachments, ...filesPaths];
      return toDetailedIssueDto(await issue.save());
    } catch (error) {
      this.logger.error(`Erreur lors de l'ajout de pièce jointe`, error.stack);
      throw error;
    }
  }

  async removeIssueAttachments(
    issueId: string,
    deleteIssueAttachmentDto: DeleteIssueAttachmentDto,
  ) {
    const { filename } = deleteIssueAttachmentDto;
    try {
      fs.stat(`public/${filename}`, (err, stats) => {
        if (err) {
          console.error(err);
        }
        fs.unlink(`public/${filename}`, (err) => {
          if (err) {
            console.error(`Le fichier n'a pas pu être supprimé :`, err);
          }
        });
      });
      return await this.issueModel.findByIdAndUpdate(issueId, {
        $pull: { attachments: filename },
      });
    } catch (error) {
      this.logger.error(
        `Erreur lors de la suppression des pièces jointes`,
        error.stack,
      );
      throw error;
    }
  }

  async updateIssueStatus(
    issueId: string,
    updateStatus: UpdateIssueStatusDto,
  ): Promise<IssueDto> {
    const { status } = updateStatus;

    const issue = await this.findIssue(issueId);
    issue.status = status;
    await issue.save();
    return this.getIssue(issueId);
  }

  async updateIssue(
    issueId: string,
    updateIssue: UpdateIssueDto,
    filesPaths: Array<string>,
  ): Promise<IssueDto> {
    const { title, content, status } = updateIssue;
    const issue = await this.findIssue(issueId);

    try {
      if (title) {
        issue.title = title;
      }
      if (content) {
        issue.content = content;
      }
      if (status) {
        issue.status = status;
      }
      if (filesPaths && filesPaths.length > 0) {
        issue.attachments = [...issue.attachments, ...filesPaths];
      }

      await issue.save();
    } catch (error) {
      this.logger.error(
        `Erreur lors de la modification du ticket`,
        error.stack,
      );
      throw error;
    }
    return this.getIssue(issueId);
  }

  async assignIssueToUser(issueId: string, userId: string) {
    return this.issueModel.findByIdAndUpdate(issueId, {
      $addToSet: { assignees: userId },
    });
  }

  async removeIssueFromUser(issueId: string, userId: string) {
    return this.issueModel.findByIdAndUpdate(issueId, {
      $pull: { assignees: userId },
    });
  }

  async associateIssueAndUser(issueId: string, assignUser: AssignUserDto) {
    const { action, assignee } = assignUser;
    const issue = await this.findIssue(issueId);
    const user = await this.userService.findUser({ _id: assignee });
    try {
      if (issue && user) {
        if (action === 'add') {
          await this.assignIssueToUser(issue._id, user._id);
          await this.userService.assignUserToIssue(user._id, issue._id);
        } else if (action === 'remove') {
          await this.removeIssueFromUser(issue._id, user._id);
          await this.userService.removeUserFromIssue(user._id, issue._id);
        }
      }
    } catch (error) {
      this.logger.error(
        `Erreur lors de l'assignation du ticket à l'utilisateur`,
        error.stack,
      );
      throw error;
    }

    return this.getIssue(issue._id);
  }

  async deleteIssue(issueId: string): Promise<any> {
    try {
      const result = await this.issueModel.deleteOne({ _id: issueId });
      if (result.deletedCount === 0) {
        throw new NotFoundException(
          `Impossible de trouver le ticket ${issueId}`,
        );
      }
      return result;
    } catch (error) {
      this.logger.error(`Erreur lors de la suppression du ticket`, error.stack);
      throw error;
    }
  }
}
