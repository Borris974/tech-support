export enum IssueStatus {
    OPENED = 'opened',
    IN_PROGRESS = 'in progress',
    CLOSED = 'closed'
  }
