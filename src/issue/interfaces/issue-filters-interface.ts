export interface IssueFilters  {
    id?: string;
    title?: string | { $regex: string };
    content?: string | { $regex: string };
    creator?: string | { $regex: string };
  }