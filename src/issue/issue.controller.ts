import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { AssignUserDto } from './dto/assign-user-dto';
import { DeleteIssueAttachmentDto } from './dto/delete-issue-attachment-dto';
import { IssueDto } from './dto/issue-dto';
import { IssueFiltersDto } from './dto/issue-filters-dto';
import { UpdateIssueDto } from './dto/update_issue-dto';
import { UpdateIssueStatusDto } from './dto/update_issue-status-dto';
import { IssueService } from './issue.service';
import { extname } from 'path';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../auth/decorators/role.decorator';
import { UserRole } from '../user/enums/user-role.enum';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Logger } from '@nestjs/common';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { User } from 'src/user/user.model';
('@nestjs/common');

@ApiTags('Issues')
@Controller('issues')
export class IssueController {
  private logger = new Logger('IssueController');
  constructor(private readonly issueService: IssueService) {}

  @Get()
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async getIssues(@Query() filters: IssueFiltersDto): Promise<Array<IssueDto>> {
    this.logger.verbose(
      `Requête GET 'issues/', filtres : ${JSON.stringify(filters)}`,
    );
    return this.issueService.getIssues(filters);
  }

  @Get('stats')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async getStatsIssues(@GetUser() user: User): Promise<any> {
    this.logger.verbose(
      `Requête GET 'issues/stats', user : ${user.email}`,
    );
    switch (user.role) {
      case UserRole.ADMIN:
      case UserRole.MANAGER:
        return this.issueService.getAdminStats();
      case UserRole.DEVELOPER:
        // TODO: implements developpers stats;       
      default:
        throw new NotFoundException(`Utilisateur non identifié`);
    }
  }

  @Get(':id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async getIssue(@Param('id') id: string): Promise<IssueDto> {
    this.logger.verbose(`Requête GET 'issues/${id}'`);
    return this.issueService.getIssue(id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  @ApiParam({ name: 'id', description: `issue's Mongo ID`, required: true })
  @UseInterceptors(
    FilesInterceptor('files', 3, {
      storage: diskStorage({
        destination: `./public/issues-attachments`,
        filename: (req, file, cb) => {
          if (
            file.mimetype.match(
              /(application\/pdf|image\/jpg|image\/jpeg|image\/png|image\/gif|text\/plain)$/,
            )
          ) {
            cb(null, `att_${Date.now()}${extname(file.originalname)}`);
          } else {
            cb(
              new HttpException(
                `Type de fichier non autorisé : ${extname(file.originalname)}`,
                HttpStatus.BAD_REQUEST,
              ),
              file.originalname,
            );
          }
        },
      }),
    }),
  )
  async updateIssue(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Param('id') issueId: string,
    @Body() updateIssue: UpdateIssueDto,
  ) {
    this.logger.verbose(`Requête PATCH 'issues/${issueId}'`);
    const filesPaths = [];

    if (files && files.length > 0) {
      files.forEach((file) => {
        filesPaths.push(`issues-attachments/${file.filename}`);
      });
    }
    return this.issueService.updateIssue(issueId, updateIssue, filesPaths);
  }

  @Patch(':id/assignees')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async assignUser(
    @Param('id') id: string,
    @Body() assignUser: AssignUserDto,
  ): Promise<IssueDto> {
    this.logger.verbose(
      `Requête PATCH 'issues/${id}/assignees', assignUser : ${JSON.stringify(assignUser)}`,
    );
    return this.issueService.associateIssueAndUser(id, assignUser);
  }

  @Patch(':id/status')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  async updateIssueStatus(
    @Param('id') id: string,
    @Body() updateStatus: UpdateIssueStatusDto,
  ): Promise<IssueDto> {
    this.logger.verbose(
      `Requête PATCH 'issues/${id}/status', updateStatus : ${JSON.stringify(updateStatus)}`,
    );
    return this.issueService.updateIssueStatus(id, updateStatus);
  }

  @Patch(':id/attachments')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('JWT')
  @ApiParam({ name: 'id', description: `issue's Mongo ID`, required: true })
  async updateIssueAttachments(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Param('id') issueId: string,
    @Body() deleteIssueAttachmentDto: DeleteIssueAttachmentDto,
  ) {
    this.logger.verbose(
      `Requête PATCH 'issues/${issueId}/attachments', deleteIssueAttachmentDto : ${JSON.stringify(deleteIssueAttachmentDto)}`,
    );
    if (deleteIssueAttachmentDto) {
      await this.issueService.removeIssueAttachments(
        issueId,
        deleteIssueAttachmentDto,
      );
    }
    return this.issueService.getIssue(issueId);
  }

  @Delete(':id')
  @UseGuards(AuthGuard(), RolesGuard)
  @ApiBearerAuth('JWT')
  @Roles(UserRole.ADMIN, UserRole.MANAGER)
  async deleteIssue(@Param('id') id: string): Promise<any> {
    this.logger.verbose(
      `Requête DELETE 'issues/${id}/`,
    );
    return this.issueService.deleteIssue(id);
  }
}
