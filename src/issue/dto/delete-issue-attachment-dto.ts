import { ApiProperty } from '@nestjs/swagger';
import { NotEquals, IsNotEmpty, IsString } from 'class-validator';

export class DeleteIssueAttachmentDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  filename: string;
}
