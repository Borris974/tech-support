import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsMongoId, IsIn } from 'class-validator';

export class AssignUserDto {

  @ApiProperty({ required: true })
  @IsString()
  @IsIn(['add', 'remove'])
  action: string;

  @ApiProperty({ required: true })
  @IsMongoId()
  @IsString()
  assignee: string;
}
