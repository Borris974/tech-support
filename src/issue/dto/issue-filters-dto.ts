import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsMongoId, IsOptional } from 'class-validator';

export class IssueFiltersDto {
  @ApiProperty({ required: false })
  @IsMongoId()
  @IsOptional()
  id: any;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  ref: any;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  title: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  content: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  status: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  creator: string;
}
