import { IsString, IsMongoId, IsArray, IsOptional } from 'class-validator';
import { toSimpleUserDto, toDetailedUserDto } from 'src/user/dto/user-dto';
import { User } from 'src/user/user.model';
import { Issue } from '../issue.model';

export class IssueDto {
  @IsMongoId()
  id: any;

  createdAt: Date | string;
  updatedAt: Date | string;

  @IsString()
  ref: string;

  @IsString()
  title: string;

  @IsString()
  content: string;

  @IsString()
  status: string;

  @IsArray()
  @IsOptional()
  assignees: Array<User>;

  @IsArray()
  attachments: Array<string>;
}

export const toDetailedIssueDto = (issue: Issue): IssueDto => {
  const {
    _id,
    createdAt,
    updatedAt,
    ref,
    title,
    content,
    status,
    creator,
    assignees,
    attachments,
  } = issue;
  return {
    id: _id,
    createdAt,
    updatedAt,
    ref,
    title,
    content,
    status,
    creator: toSimpleUserDto(creator),
    assignees: assignees.map((user) => {
      return toSimpleUserDto(user);
    }),
    attachments,
  } as unknown as IssueDto;
};

export const toSimpleIssueDto = (issue: Issue): IssueDto => {
  const {
    _id,
    createdAt,
    updatedAt,
    ref,
    title,
    content,
    status,
    creator,  
    attachments,
  } = issue;
  return {
    id: _id,
    createdAt,
    updatedAt,
    ref,
    title,
    content,
    status,
    creator: toSimpleUserDto(creator),
    attachments,
  } as unknown as IssueDto;
};
