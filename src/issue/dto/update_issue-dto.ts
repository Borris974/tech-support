import { ApiProperty } from '@nestjs/swagger';
import { NotEquals, IsNotEmpty, IsString, IsIn, IsOptional } from 'class-validator';
import { IssueStatus } from '../enums/status.enum';

export class UpdateIssueDto {

  @ApiProperty({ required: false})
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsOptional()
  title: string;

  @ApiProperty({ required: false})
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsOptional()
  content: string;

  @ApiProperty({ required: false, enum: IssueStatus })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsOptional()
  @IsIn([IssueStatus.OPENED, IssueStatus.IN_PROGRESS, IssueStatus.CLOSED])
  status: IssueStatus;
}