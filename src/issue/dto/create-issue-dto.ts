import { ApiProperty } from '@nestjs/swagger';
import { NotEquals, IsNotEmpty, IsString } from 'class-validator';

export class CreateIssueDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  title: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  content: string;
}
