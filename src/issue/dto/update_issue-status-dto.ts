import { ApiProperty } from '@nestjs/swagger';
import { NotEquals, IsNotEmpty, IsString, IsIn } from 'class-validator';
import { IssueStatus } from '../enums/status.enum';

export class UpdateIssueStatusDto {

  @ApiProperty({ required: true, enum: IssueStatus })
  @IsNotEmpty()
  @NotEquals('')
  @IsString()
  @IsIn([IssueStatus.OPENED, IssueStatus.IN_PROGRESS, IssueStatus.CLOSED])
  status: IssueStatus;
}